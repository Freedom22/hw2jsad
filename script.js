

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон", 
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];

   const div = document.getElementById('root');
   const ul = document.createElement('ul');
   
   books.forEach(book => {
       if(book.author && book.name && book.name){
           const liElem = document.createElement('li');
           liElem.textContent = `${book.author}, ${book.name}, ${book.price}`;
           ul.appendChild(liElem);
    }else{
           console.error(`Error ${JSON.stringify(book)}`)
    }

   });
   div.appendChild(ul);

